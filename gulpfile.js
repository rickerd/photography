'use strict';

const gulp = require('gulp');
const imageResize = require('gulp-image-resize');
const sass = require('gulp-sass')(require('sass'));
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const del = require('del');
const debug = require('gulp-debug');
const fs = require('fs');
const path = require('path');

gulp.task('resize-images', () => {
    let scriptsPath = './images';

    function getFolders (dir) {
        return fs.readdirSync(dir)
            .filter(function (file) {
                return fs.statSync(path.join(dir, file)).isDirectory();
            });
    }

    return getFolders(scriptsPath).map(folder => {
        return gulp.src([
                `${scriptsPath}/${folder}/fulls/*.jpg`,
            ])
            .pipe(imageResize({
                width: 512,
                imageMagick: true
            }))
            .pipe(gulp.dest(`${scriptsPath}/${folder}/thumbs`));
    });
});


gulp.task('delete', function () {
    return del(['images/*.*']);
});

// compile scss to css
gulp.task('sass', function () {
    return gulp.src('./assets/sass/main.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename({basename: 'main.min'}))
        .pipe(gulp.dest('./assets/css'));
});

// watch changes in scss files and run sass task
gulp.task('sass:watch', function () {
    gulp.watch('./assets/sass/**/*.scss', ['sass']);
});

// minify js
gulp.task('minify-js', function () {
    return gulp.src('./assets/js/main.js')
        .pipe(uglify())
        .pipe(rename({basename: 'main.min'}))
        .pipe(gulp.dest('./assets/js'));
});

// build task
gulp.task('build', gulp.series('sass', 'minify-js'));

// resize images
gulp.task('resize', gulp.series('resize-images', 'delete'));

// default task
gulp.task('default', gulp.series('build', 'resize'));
